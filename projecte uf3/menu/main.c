#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define MAX_NOM 35
#define MAX_POBLACIO 50
#define BB while(getchar()!='\n')
#define MSS_ERROR_SELECCION "eloigeix un num del 1  al 3 !!!"
#define MAX_COS 10000
struct Persona{
    char nom[MAX_NOM+1];
    int edat;
    char poblacio[MAX_POBLACIO+1];
};
void entradaPersona(struct Persona *p);
void imprimirPersona(struct Persona p);
int ecriurePersonaDisc(FILE *fitxer,struct Persona *p, int nReg);
int llegirPersonaDisc(FILE *fitxer,struct Persona *p, int nReg);
void imprimirMenu();
void rebOpcio(int *opc);
void imprimirError(char mss[]);
bool volsContinuar();
void llegirDocument(FILE *fitxer);
void htmlPersona(FILE *fitxer,FILE *filehtml);

int main()
{
    struct Persona p;
    FILE *fitxer=NULL;
    FILE *fitxerHtml=NULL;
    int opcio = 0;
    while(opcio != 9){
        imprimirMenu();
        rebOpcio(&opcio);
            if(opcio > 3  && opcio != 9){
                imprimirError(MSS_ERROR_SELECCION);
        }else{
            switch(opcio){
                case 1:
                    do{
                    entradaPersona(&p);
                    ecriurePersonaDisc(fitxer, &p, 1);
                    }while (volsContinuar());
                    break;
                case 2:
                    llegirDocument(fitxer);
                    break;
                case 3:
                    htmlPersona(fitxer,fitxerHtml);

                    break;
            }
        }
    }


    return 0;

}
bool volsContinuar(){
    char q;
    printf("Vols continuar?(s/n)");
    scanf("%c",&q);BB;
    if (q =='s'){
        return true;
    }else{
        return  false;
    }
}

void imprimirError(char mss[]){
    printf("   ---   ---   ---   ---   ---   \n");
    printf("*  %s  *\n", mss);
    printf("   ---   ---   ---   ---   ---   \n");
}
void rebOpcio(int *opc){
    scanf("%d", opc); BB;
}

void imprimirMenu(){
    printf(" **   MENU   ** \n");
    printf("    1. Alta\n");
    printf("    2. Listar por pantalla\n");
    printf("    3. Listar en p�gina web\n\n");
    printf("    9. Salir\n\n");
    printf("    Elige la opcion 1-3\n");
}
void entradaPersona(struct Persona *p){
    printf("Nom: ");
    scanf("%35[^\n]",p->nom);BB;// (*p).nom
    printf("Edat: ");
    scanf("%i",&p->edat);BB;// &(*p).edat
    printf("Poblaci�: ");
    scanf("%50[^\n]",p->poblacio);BB;// (*p).poblacio
}

void imprimirPersona(struct Persona p){
    printf("Nom: %s\n",p.nom);
    printf("Edat: %i\n", p.edat);
    printf("Poblaci�: %s\n", p.poblacio);
}

int ecriurePersonaDisc(FILE *fitxer,struct Persona *p, int nReg){
    int n,error=0;
    fitxer=fopen("menu.bin","ab");
    //escriure les dades de la persona al fitxer persones.bin
    n=fwrite(p,sizeof(struct Persona),nReg,fitxer);
    if(n!=nReg){
        error=2; //error numero registres escrits
    }
    fclose(fitxer);
    return error;
}

/*
Retorn de tipus enter. Interpretaci�:
    0 -> no error. Hi ha les dades llegides del fitxer al par�metre *p
    1 -> error obrir fitxer
    3 -> error de lectura
    4 -> Advert�ncia. Ha arribat al final del fitxer.
*/
int llegirPersonaDisc(FILE *fitxer,struct Persona *p, int nReg){
    int n,error=0;
    //escriure les dades de la persona al fitxer persones.bin
    n=fread(p,sizeof(struct Persona),nReg,fitxer);
    if(!feof(fitxer)){
        if(n==0){
            error=3; //error de lectura
        }
    }else{
        error=4 ; //�s un av�s. Ha arribat al final del fitxer.
    }
    return error;
}
void llegirDocument(FILE *fitxer){
    int error=0,n;
    struct Persona p;
    fitxer=fopen("menu.bin","rb");
    if (fitxer==NULL){
        error=1;
    }
    while(!feof(fitxer)){
        n=llegirPersonaDisc(fitxer,&p,1);
        if (n==0){
            imprimirPersona(p);
        }
    }
    fclose(fitxer);

}
void generarHtml(struct Persona *p, char cadenaAux[100]){
    char aux[100];

    strcat(cadenaAux, "<tr><td>");
    strcat(cadenaAux, p->nom);
    strcat(cadenaAux, "</td><td>");
    sprintf(aux, "%d", p->edat); //Convertir numero a cadena
    strcat(cadenaAux, aux);
    strcat(cadenaAux, "</td><td>");
    strcat(cadenaAux, p->poblacio);
    strcat(cadenaAux, "</td></tr>");

}
void htmlPersona(FILE *fitxer,FILE *fitxerhtml){
    struct Persona p;
    int error=0,n;
    char cadenaTmp[100];
    char cap[]="<!DOCTYPE html> \
            <html lang=\"en\"> \
            <head> \
                <meta charset=\"UTF-8\"> \
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
                <title>Document</title> \
            </head> \
            <body> \
            <table border=1><tr><th>Nom</th><th>Edat</th><th>Poblaci�</th></tr>";

        char cos[MAX_COS]="";

        char peu[]="</body>\
                </html>";



        fitxer= fopen("menu.bin","rb");//fitxer binari (b) d'afegir (a)
        if(fitxer==NULL){
            error=1; //error obrir fitxer
        }else{
            fitxerhtml = fopen("menu.html", "w");
            if(fitxerhtml==NULL){
                error = 1;
            }else{
                error = fputs(cap, fitxerhtml);
            if(error == EOF){
                error = 2;
            }

             while(!feof(fitxer)){
                n=llegirPersonaDisc(fitxer, &p, 1);
                    if(n==0){
                    buidarCadena(cadenaTmp,100);
                    generarHtml(&p, cadenaTmp);
                    strcat(cos, cadenaTmp);
                }
                if(n==1){
                    printf("\nError en obrir el fitcer...\n");break;
                }
                if(n==3){
                    printf("\nError en obrir el fitxer...\n");break;
                }
            }

            error = fputs(cos, fitxerhtml);
            if(error == EOF){
                error = 2;
            }

            error = fputs(peu, fitxerhtml);
            if(error == EOF){
                error = 2;
            }


            if(fclose(fitxerhtml)){
                error = 3;
            }

            if(fclose(fitxer)){
                error = 3;
                }
        }
    }
}
void buidarCadena(char cadena[],int u ){
    int i=0;
    while (i<u){
        cadena[i]='\0';
        i++;
    }
}

